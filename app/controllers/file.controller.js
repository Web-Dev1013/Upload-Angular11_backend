const db = require("../models");
const Op = db.Sequelize.Op;
const Image = db.image;
const uploadFile = require("../middleware/upload");
const fs = require("fs");
const {
  response
} = require("express");

const baseUrl = "http://localhost:8080/api/files/";

exports.getListFiles = (req, res) => {
  // find all image information from 
  try {
    Image.findAll({
        attributes: ['id', 'fileName', 'fileUrl', 'myDate']
      })
      .then(imageInfos => {
        res.status(200).json({
          message: "Get Images' Infos!",
          imageInfos: imageInfos
        });
      })
  } catch (error) {
    // log on console
    console.log(error);

    res.status(500).json({
      message: "Retrieving Error!",
      error: error
    });
  }
};

exports.upload = async (req, res) => {
  try {
    console.log("++++++++++++++", req.body.id)
    await uploadFile(req, res);

    if (req.body == undefined) {
      return res.status(400).send({
        message: "Please upload a file!"
      });
    }

    const file_info = {
      fileName: req.file.originalname,
      fileUrl: "../../assets/upload_files/" + req.file.originalname
    };
    if (req.body.id == 0) {
      //Save Image in the database
      Image.create(file_info)
        .then(data => {
          const response = "success";
          res.send(response);
        })
        .catch(err => {
          res.status(500).send({
            message: err.message || "Some error occurred while creating the Experience."
          });
        });
    } else {
      Image.findOne({
        where: {
          id: req.body.id
        }
      }).then(
        image => {
          if (!image) {
            return res.status(404).send({
              message: "User not found"
            });
          }
          Image.update({
            fileName: req.file.originalname,
            fileUrl: "../../assets/upload_files/" + req.file.originalname
            }, {
              where: {
                id: req.body.id
              }
            })
            .then(num => {
              if (num == 1) {
                res.send({
                  message: "User was updated successfully."
                });
              } else {
                res.send({
                  message: `Cannot update User with id=${id}. Maybe User was not found or req.body is empty!`
                });
              }
            })
            .catch(err => {
              res.status(500).send({
                message: err.message || "Error updating User with id=" + id
              });
            });
        })
    }

  } catch (err) {
    if (err.code == "LIMIT_FILE_SIZE") {
      return res.status(500).send({
        message: "File size cannot be larger than 2MB!",
      });
    }
    res.status(500).send({
      message: `Could not upload the file: ${err}`,
    });
  }
};

// exports.createSave = async(req, res) => {

//   await uploadFile(req, res);
//   //create a Upload table
//   const file_info = {
//     fileName: req.body,
//     fileUrl: "../../assets/upload_files/" + req.body
//   };

//   //Save Experience in the database
//   Image.create(file_info)
//     .then(data => {
//       response = uploadFile(req, res);
//       res.send(response);
//     })
//     .catch(err => {
//       res.status(500).send({
//         message: err.message || "Some error occurred while creating the Experience."
//       });
//     });
// }
// module.exports = {
//   createSave,
//   // getListFiles,
//   // download,
// };