module.exports = (sequelize, Sequelize) => {
    const Image = sequelize.define("images", {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false         
      },
      fileName: {
        type: Sequelize.STRING,
        allowNull: false
      },
      fileUrl: {
        type: Sequelize.STRING,
        allowNull: false
      },
      myDate: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
      },
      createdAt: Sequelize.DATE,
      updatedAt: Sequelize.DATE,
    });
  return Image;
  };